#!/bin/sh

#Pre-requisites
apt-get update -y
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common


#Juniper PyEZ
apt-get update -qq && apt-get install -y -qq tree python-pip python-yaml python-paramiko

pip install --upgrade pip setuptools
pip install -r requirements.txt




#Key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88

#New repo
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

#Instalation
apt-get update -y
apt-get install -y docker-ce

#Test
docker run hello-world


groupadd docker
usermod -aG docker $USER

systemctl enable docker
