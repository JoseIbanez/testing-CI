#!/usr/bin/env python

#Libraries
import time
import logging
import json
import datetime
import yaml
import re
import argparse
import os

#Local modules
import func




logging.basicConfig()
logger = logging.getLogger(__name__)
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)


def main():

    #Get options
    parser = argparse.ArgumentParser(
        description='Request any Twitter Streaming or REST API endpoint')
 
    parser.add_argument('-f1',     action='store_true')


    parser.add_argument(
        '-val1',
        type=str,
        help='value 1',
        default="")

    parser.add_argument(
        '-val2',
        type=str,
        help='value 2',
        default="")

    args = parser.parse_args()




    if args.f1:
        logger.debug("f1: "+args.val1+" "+args.val2)
        ret=func.f1(args.val1, args.val2)
        logger.debug(ret)





if __name__ == "__main__":
    main()